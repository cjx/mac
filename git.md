#Git global setup
```
git config --global user.name "cjx"
git config --global user.email "185506034@qq.com"
```
#Create a new repository
```
mkdir mac
cd mac
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitlab.com/cjx/mac.git
git push -u origin master
```
#Push an existing Git repository
```
cd existing_git_repo
git remote add origin https://gitlab.com/cjx/mac.git
git push -u origin master
```